package co.lorenzi.wordfrequencies;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFrequenciesTest {

	@Test
	public void testTokeniser() {
		String line = "This is a   test";
		String[] tokens = new WordFrequencies().getTokens(line);
		assertArrayEquals(new String[] { "this", "is", "a", "test" }, tokens);
	}

	@Test
	public void testCounts() {
		String[] tokens = new String[] { "a", "mate", "material", "may", "maybe", "right", "maybe" };
		Map<String, Integer> counts = new WordFrequencies().getCounts(tokens);
		
		Map<String, Integer> expected = new HashMap<>();
		expected.put("maybe", 2);
		expected.put("right", 1);
		expected.put("material", 1);
		
		assertEquals(expected, counts);
	}

}
