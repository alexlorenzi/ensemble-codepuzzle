package co.lorenzi.wordfrequencies;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class WordFrequencies {
	// Simple tokeniser splits words at whitespace
	private static Pattern TOKENISER = Pattern.compile("(\\W+)");

	/**
	 */
	void printWordFrequencies(String text) {
		String[] tokens = getTokens(text);
		Map<String, Integer> counts = getCounts(tokens);
		
		// Sort by most frequent and print
		counts
			.entrySet()
			.stream()
			.sorted((a,b) -> (Integer.compare(b.getValue(), a.getValue())))
			.forEach(System.out::println);
	}

	/**
	 */
	String[] getTokens(String text) {
		String[] words = TOKENISER.split(text.toLowerCase());
		return words;
	}

	/**
	 */
	Map<String, Integer> getCounts(String[] words) {
		// By sorting words by length we reduce the number of comparisons by only
		// having to look at words to the right of us, as shorter words can't be
		// substrings
		Arrays.sort(words, (s1, s2) -> (s1.length() - s2.length()));

		Map<String, Integer> counts = IntStream.range(0, words.length)
			.filter(i -> {
				// TODO Could be improved here by not repeatedly checking equal words
				return IntStream.range(i + 1, words.length)
						.noneMatch(j -> !words[j].equals(words[i]) && words[j].contains(words[i]));
			})
			.mapToObj(i -> words[i])
			.collect(toMap(w -> w, w -> 1, Integer::sum));

		return counts;
	}

	/**
	 */
	private static void printHelp() {
		System.err.println("USAGE");
		System.err.println("\tjava " + WordFrequencies.class.getCanonicalName() + " filename");
	}

	/**
	 */
	public static void main(String[] args) {
		// Check filename passed
		if (args.length < 1) {
			printHelp();
		}

		try (Stream<String> st = Files.lines(Paths.get(args[0]))) {

			String text = st.collect(joining(" "));
			new WordFrequencies().printWordFrequencies(text);

		} catch (IOException e) {
			System.err.println("Error reading file: " + e.getMessage());
			System.exit(-1);
		}
	}

}
